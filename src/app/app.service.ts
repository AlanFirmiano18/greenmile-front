import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Evento } from './_models/evento';

@Injectable()
export class AppService {

  evento:Evento;

  constructor(private http: Http) {
    localStorage.setItem('url','http://localhost:8080');
  }
  
  public deslogar(){
    localStorage.removeItem('token');
  }
}
