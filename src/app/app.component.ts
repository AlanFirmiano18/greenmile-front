import { Component, DoCheck, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { Router } from '@angular/router';
import { LoginService } from './login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck,OnInit{
  title = 'GreenMile';
  lista = [];

  ngOnInit(){
    localStorage.removeItem('token');
  }
  mostrarMenu:boolean = false;
    constructor(private service: AppService, private router:Router, private login:LoginService){
  }

  autenticado(){
    return localStorage.getItem('token')?true:false;
  }

  ngDoCheck(){
    this.mostrarMenu = localStorage.getItem('token')? true : false; 
  }

  deslogar(){
    localStorage.removeItem('token');
  }
}
