import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { NotfoundComponent } from '../notfound/notfound.component';
import { EventoComponent } from '../evento/evento.component';

const APP_ROUTES: Routes = [
  
  {path: 'evento', loadChildren: 'app/evento/evento.module#EventoModule'},
  {path: 'equipe', loadChildren: 'app/equipe/equipe.module#EquipeModule'},
  {path: 'login', loadChildren: 'app/login/login.module#LoginModule'},
  {path: '', redirectTo: 'evento', pathMatch: 'full'}
]

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
