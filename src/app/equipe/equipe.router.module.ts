import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { EquipeComponent } from './equipe.component';
import { AdicionarEquipeComponent } from './adicionar/adicionar.equipe.component';

const APP_ROUTES: Routes = [
  {path:"", component:EquipeComponent},
  {path:"adicionar", component:AdicionarEquipeComponent}
]

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule]
})
export class EquipeRoutingModule {

}