import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { LoginService } from '../../login/login.service';
import { Evento } from '../../_models/evento';
import { EquipeService } from './../equipe.service';
import { ToastService } from '../../toast.service';
import { Usuario } from '../../_models/usuario';
import { Equipe } from '../../_models/equipe';
import { AppService } from '../../app.service';
import { Participante } from '../../_models/participante';

@Component({
  selector: 'app-adicionar-equipe',
  templateUrl: './adicionar.equipe.component.html',
  styleUrls: ['./adicionar.equipe.component.css']
})
export class AdicionarEquipeComponent implements OnInit {

  constructor(private appService: AppService,private route: ActivatedRoute,private servico: EquipeService, private toastService: ToastService, private rota: Router) { }
  equipe:Equipe = new Equipe();
  evento: Evento = new Evento();
  //inscricao: Subscription;
  id:number;
  usuario:Usuario = new Usuario();
  tamanho : Participante[];
  ngOnInit() {
    this.evento = this.appService.evento;
    this.tamanho = new Array(this.evento.numeroParticipantes);
    for(let i of this.tamanho){
      i = new Participante();
      
    }
  }
  save(){
    this.servico.salvar(this.equipe,this.evento.id).subscribe(
      res => {
        this.toastService.toast(res,"green");
        this.rota.navigateByUrl('eventos');
      },

      err => this.toastService.toast(err,"red")
    );
  }
}
