import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarEquipeComponent } from './adicionar.equipe.component';

describe('AdicionarEventoComponent', () => {
  let component: AdicionarEquipeComponent;
  let fixture: ComponentFixture<AdicionarEquipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarEquipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarEquipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
