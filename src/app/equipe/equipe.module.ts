import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquipeComponent } from './equipe.component';
import { MaterializeModule } from 'angular2-materialize';
import { FormsModule } from '@angular/forms';
import { EquipeRoutingModule } from './equipe.router.module';
import { EquipeService } from './equipe.service';
import { ToastService } from '../toast.service';
import { EventoComponent } from '../evento/evento.component';
import { AdicionarEquipeComponent } from './adicionar/adicionar.equipe.component';

@NgModule({
  imports: [
    CommonModule,
    MaterializeModule,
    FormsModule,
    EquipeRoutingModule
  ],
  providers: [EquipeService, ToastService],
  declarations: [
    EquipeComponent,
    AdicionarEquipeComponent
  ],
  exports: [
    EquipeComponent,
    AdicionarEquipeComponent
  ]
})
export class EquipeModule { }
