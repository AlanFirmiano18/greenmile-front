import { Injectable } from '@angular/core';
import { Http,RequestOptions,Headers } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Equipe } from '../_models/equipe';
import { Evento } from '../_models/evento';
@Injectable()
export class EquipeService {

  public equipe: Equipe = null;

  private url:string = localStorage.getItem('url') + '/equipes';

  private options:RequestOptions;
  constructor(private http: Http) {
    let token = localStorage.getItem("token");
      let header = new Headers();
      header.append('Content-Type', 'application/json');
      header.append('Authorization', token);
      this.options = new RequestOptions({ headers: header });
  }

  public salvar(equipe: Equipe, id:number): Observable<string>{
    return this.http.post(this.url+"/"+id, equipe).map(
      (res) => res.text()
      ,
      (err) => err.text()
    );
  }
  public editar(equipe: Equipe): Observable<string>{
    return this.http.put(this.url, equipe,this.options).map(
      (res) => res.text()
      ,
      (err) => err.text()
    );
  }
  public remover(equipe: Equipe): Observable<string>{
    return this.http.delete(this.url + '/' + equipe.id,this.options).map(
      (res) => res.text()
      ,
      (err) => err.text()
    );
  }
}