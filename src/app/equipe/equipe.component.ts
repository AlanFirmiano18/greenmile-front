import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastService } from '../toast.service';
import { EquipeService } from './equipe.service';
import { Equipe } from '../_models/equipe';

@Component({
  selector: 'app-equipe',
  templateUrl: './equipe.component.html',
  styleUrls: ['./equipe.component.css']
})
export class EquipeComponent implements OnInit {

  constructor(private servico: EquipeService, private toastService: ToastService, private rota: Router) { }
  equipe:Equipe = new Equipe();
  ngOnInit() {
  }

  
}
