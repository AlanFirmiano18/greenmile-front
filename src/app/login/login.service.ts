import { Injectable, EventEmitter } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Usuario } from '../_models/usuario';

@Injectable()
export class LoginService {
  private url = localStorage.getItem('url') + '/responsible';

  logado:boolean = false;
  mostrarMenuEmitter = new EventEmitter<boolean>();

  constructor(private http: Http) { }
  private options:RequestOptions;

  public logar(usuario:Usuario) : Observable<string>{
    return this.http.post(localStorage.getItem('url') + '/login', usuario)
    .map(res => {
      localStorage.setItem('token', res.text());
      let token = localStorage.getItem("token");
      let header = new Headers();
      header.append('Content-Type', 'application/json');
      header.append('Authorization', token);
      this.options = new RequestOptions({ headers: header });
      this.logado = true;
      this.mostrarMenuEmitter.emit(true);
      return "sucesso";
    },
    err=>{
      this.logado = false;
      this.mostrarMenuEmitter.emit(false);
    });
  }

  usuarioEstaAutenticado(){
    return this.logado;
  }

  public salvarOrganizador(usuario: Usuario) : Observable<string>{
    return this.http.post('http://localhost:8080/organizadores/',usuario).map(
      (res) => res.text()
      ,
      (err) => err.text()
    )
  }

  public remover(usuario: Usuario) : Observable<string>{
    return this.http.delete('http://localhost:8080/organizadores/'+usuario.id).map(
      (res) => res.text()
      ,
      (err) => err.text()
    );
  }

    public listar () : Observable<any>{
      return this.http.get('http://localhost:8080/organizadores').map(
        (res) => res.json()
        ,
        function(err){

        }
      );
    }
}
