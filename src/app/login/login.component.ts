import { Component, OnInit, Renderer2 } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';
import { Usuario } from '../_models/usuario';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  constructor(private servico: LoginService, private rota: Router, private render: Renderer2) { }

  ngOnInit() {
    
  }
  private usuario: Usuario = new Usuario();
  logar() {
    this.servico.logar(this.usuario).subscribe(
      res => {

        this.rota.navigate(["evento"]);
      },
      err => {

      }
    )

  }
}
