import { Organizador } from "./organizador";

export class Evento{
    id:number;
    nome:string;
    descricao:string;
    local:string;
    data:Date;
    numeroParticipantes:number;
    numeroEquipes:number;
    status:boolean;
    organizadores:Organizador[];
}