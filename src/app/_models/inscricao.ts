import { Equipe } from "./equipe";
import { Evento } from "./evento";

export class Inscricao{
    id:number;
    data:Date;
    equipe: Equipe = new Equipe();
    evento: Evento = new Evento();
}