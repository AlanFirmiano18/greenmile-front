import { Participante } from "./participante";

export class Equipe{
    id:number;
    nome:string;
    participantes:Participante[];
}