export class Participante {
    id:number;
    email:string;
    password:string;
    nome:string;
    pathFoto:string;
    telefone:string;
    tamanho:string;
    papeis:string[];
}