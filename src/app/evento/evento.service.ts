import { Injectable } from '@angular/core';
import { Http,RequestOptions,Headers } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Evento } from '../_models/evento';
@Injectable()
export class EventoService {

  public evento: Evento = null;

  private url:string = localStorage.getItem('url') + '/eventos';

  private options:RequestOptions;
  constructor(private http: Http) {
    let token = localStorage.getItem("token");
      let header = new Headers();
      header.append('Content-Type', 'application/json');
      header.append('Authorization', token);
      this.options = new RequestOptions({ headers: header });
  }

  public salvar(evento: Evento): Observable<string>{
    return this.http.post(this.url, evento,this.options).map(
      (res) => res.text()
      ,
      (err) => err.text()
    );
  }
  public editar(evento: Evento): Observable<string>{
    return this.http.put(this.url, evento,this.options).map(
      (res) => res.text()
      ,
      (err) => err.text()
    );
  }
  public remover(evento: Evento): Observable<string>{
    return this.http.delete(this.url + '/' + evento.id,this.options).map(
      (res) => res.text()
      ,
      (err) => err.text()
    );
  }

  public disponiveis (): Observable<any> {
    return this.http.get(this.url + '/disponiveis').map(
      (res) => res.json()
      ,
      function(err){
        console.log(err);
      }
    );
  }

  public listar (): Observable<any> {
    return this.http.get(this.url + '/disponiveis',this.options).map(
      (res) => res.json()
      ,
      function(err){
        console.log(err);
      }
    );
  }
}