import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { EventoComponent } from './evento.component';
import { AdicionarEventoComponent } from './adicionar/adicionar.evento.component';

const APP_ROUTES: Routes = [
  {path:"", component:EventoComponent},
  {path:"adicionar/:id", component:AdicionarEventoComponent}
]

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule]
})
export class EventoRoutingModule {

}