import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MaterializeModule } from 'angular2-materialize';
import { ToastService } from '../toast.service';
import { EventoRoutingModule } from './evento.router.module';
import { EventoService } from './evento.service';
import { EventoComponent } from './evento.component';
import { AdicionarEventoComponent } from './adicionar/adicionar.evento.component';
@NgModule({
  imports: [
    CommonModule,
    MaterializeModule,
    FormsModule,
    EventoRoutingModule,
  ],
  providers: [EventoService, ToastService],
  declarations: [
    EventoComponent,
    AdicionarEventoComponent
  ],
  exports: [
    EventoComponent,
    AdicionarEventoComponent
  ]
})
export class EventoModule { }
