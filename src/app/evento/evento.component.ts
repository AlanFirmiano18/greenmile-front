import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';
import { Evento } from '../_models/evento';
import { EventoService } from './evento.service';
import { ToastService } from '../toast.service';
import { Usuario } from '../_models/usuario';
import { AppService } from '../app.service';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css']
})
export class EventoComponent implements OnInit {

  constructor(private appService :AppService, private servico: EventoService, private toastService: ToastService, private rota: Router) { }
  eventos:Evento[] = [];
  eventosAll:Evento[] = [];
  usuario:Usuario = new Usuario();
  ngOnInit() {
    if(!this.autenticado)
      this.listar();
    this.listarTodos();
  }
  autenticado(){
    if(localStorage.getItem('token')){
      this.listarTodos();
      return true;
    }else{
      this.listar();
      return false;
    }
  }
  redirecionar(evento : Evento){
    this.appService.evento = evento
    this.rota.navigateByUrl('equipe/adicionar');
  }
  listarTodos(){
    this.servico.listar().subscribe(
      res => this.eventos = res,
      err => console.log(err)
    );
  }
  listar(){
    this.servico.disponiveis().subscribe(
      res => this.eventos = res,
      err => console.log(err)
    );
  }

}
