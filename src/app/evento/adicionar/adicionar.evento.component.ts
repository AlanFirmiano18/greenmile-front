import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { LoginService } from '../../login/login.service';
import { Evento } from '../../_models/evento';
import { EventoService } from './../evento.service';
import { ToastService } from '../../toast.service';
import { Usuario } from '../../_models/usuario';
import { Equipe } from '../../_models/equipe';

@Component({
  selector: 'app-adicionar-evento',
  templateUrl: './adicionar.evento.component.html',
  styleUrls: ['./adicionar.evento.component.css']
})
export class AdicionarEventoComponent implements OnInit {

  constructor(private route: ActivatedRoute,private servico: EventoService, private toastService: ToastService, private rota: Router) { }
  equipe:Equipe = new Equipe();
  evento: Evento = new Evento();
  inscricao: Subscription;
  id:number;
  usuario:Usuario = new Usuario();
  ngOnInit() {
    this.inscricao = this.route.params.subscribe((params:any) => {
      this.id = params['id'];
      console.log(this.id);
    });
  }

  
  ngOnDestroy(){
    this.inscricao.unsubscribe();
  }
}
